package org.nrg.xsync.services.local;

/**
 * @author Mohana Ramaratnam
 *
 */
public interface WeeklySyncService {
	void syncWeekly();
}
